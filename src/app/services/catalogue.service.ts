import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {UserService} from "./user.service";

@Injectable({
  providedIn: 'root'
})
export class CatalogueService {
  private pokemons: PokemonObject[] = []
  private next = "https://pokeapi.co/api/v2/pokemon/"
  private error: string = ''


  constructor(private readonly http: HttpClient) {
  }

  public fetchPokemons(): void {
    this.http.get<PokemonFetch>(this.next)
      .subscribe(r => {
        let newPokemons = this.getPokemonObjects(r.results)
        newPokemons = newPokemons.filter(it => {
          for (let pokemon of UserService.getAcquiredPokemon()) {
            it.id === pokemon.id ? it.selected = true : {}
          }
          return it
        })
        this.pokemons.push(...newPokemons)
        this.next = r.next
      }, (error: HttpErrorResponse) => {
        this.error = error.message
      })
  }

  public updatePokemonList(pokemon: PokemonObject) : void {
    this.pokemons = this.pokemons.map(it => {
      if (it.id === pokemon.id) {
        it.selected = pokemon.selected
      }
      return it
    })
  }

  private getPokemonObjects(results: []) {
    return results.map(it => {
      return this.getPokemonObject(it)
    })
  }

  private getPokemonObject(obj: any): PokemonObject {
    const splitUrl = obj.url.split("/")
    return {
      name: obj.name,
      id: Number(splitUrl[splitUrl.length - 2]),
      selected: false
    }
  }

  public getPokemons(): PokemonObject[] {
    if (UserService.getAcquiredPokemon() === []) {
      return this.pokemons.filter(it => it.selected = false)
    }
    return this.pokemons.map(it => {
      for (let p of UserService.getAcquiredPokemon()) {
        if (it.id === p.id) {
          it.selected = true
          return it
        }
      }
      it.selected = false
      return it
    })
  }
}
