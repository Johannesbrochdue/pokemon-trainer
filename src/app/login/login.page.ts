import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit {
  constructor(private readonly router: Router, private readonly userService: UserService) {
  }

  // @ts-ignore
  ngOnInit() {
    if (!UserService.noUser()) {
      this.router.navigate(['catalogue'])
    }
  }

  public onSubmit(loginForm: NgForm): Promise<Boolean> {
    this.userService.loginUser(loginForm.value.username)
    return this.router.navigate(['catalogue'])
  }
}
