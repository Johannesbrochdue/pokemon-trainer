import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {LoginPage} from "./login/login.page";
import {AppRoutingModule} from "./app-routing.module";
import {FormsModule} from "@angular/forms";
import {CataloguePage} from "./catalogue/catalogue.page";
import {HttpClientModule} from "@angular/common/http";
import {CatalogueItemComponent} from "./catalogue-item/catalogue-item.component";
import {TrainerPage} from "./trainer/trainer.page";

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    CataloguePage,
    CatalogueItemComponent,
    TrainerPage
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
