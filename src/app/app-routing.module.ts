import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LoginPage} from "./login/login.page";
import {CataloguePage} from "./catalogue/catalogue.page";
import {TrainerPage} from "./trainer/trainer.page";

//login-page

const routes: Routes = [
  {
    path: "",
    pathMatch: 'full',
    redirectTo: '/login'
  },
  {
    path: 'login',
    component: LoginPage
  },
  {
    path: 'catalogue',
    component: CataloguePage
  },
  {
    path: 'trainer',
    component: TrainerPage
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
