interface PokemonFetch {
  count: number,
  next: string,
  previous: string | null,
  results: []
}

interface PokemonObject {
  name: string,
  id: number,
  selected: boolean
}

interface User {
  username: string,
  acquired_pokemon: PokemonObject[]
}
